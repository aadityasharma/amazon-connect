import { FAAppletClient } from './lib.js';

const SERVICE = {
    name: 'FreeAgentService',
    appletId: `aHR0cHM6Ly9hYWRpdHlhc2hhcm1hLmdpdGxhYi5pby9hbWF6b24tY29ubmVjdC9zY3JlZW4uaHRtbA==`
};

export const FAClient = new FAAppletClient({
    appletId: SERVICE.appletId,
});

export function openApplet() {
    FAClient.open();
};

export function closeApplet() {
  FAClient.close();
};