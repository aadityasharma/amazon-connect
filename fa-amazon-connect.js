import {
	FAClient,
	openApplet,
	closeApplet
} from './util.js';

/* Edit the instanceName below with your instance alias */
let instanceName = "freeagent-dev";

export async function startupService() {
	console.log('Starting up amazon connect!');
	// $('#containerDiv').hide();

	let containerDiv = document.getElementById('containerDiv');

	connect.core.initCCP(containerDiv, {
		ccpUrl: "https://" + instanceName + ".awsapps.com/connect/ccp-v2/",
		loginPopup: true,
		loginPopupAutoClose: true, // optional, defaults to `false`
		loginOptions: { // optional, if provided opens login in new window
			autoClose: true, // optional, defaults to `false`
			height: 600, // optional, defaults to 578
			width: 400, // optional, defaults to 433
			top: 0, // optional, defaults to 0
			left: 0 // optional, defaults to 0
		},
		region: "us-west-2", // REQUIRED for `CHAT`, optional otherwise
		softphone: { // optional, defaults below apply if not provided
			allowFramedSoftphone: true, // optional, defaults to false
			disableRingtone: false, // optional, defaults to false
			ringtoneUrl: "./ringtone.mp3" // optional, defaults to CCP’s default ringtone if a falsy value is set
		},
		pageOptions: { // optional
			enableAudioDeviceSettings: false, // optional, defaults to 'false'
			enablePhoneTypeSettings: true // optional, defaults to 'true'
		}
	});
	connect.contact(function (contact) {
		contact.onConnecting(function (contact) {
			// $('#loading-text').hide();
			// $('#containerDiv').show();

			var attributeMap = contact.getAttributes();
			var baseURL = attributeMap.screenPopURL.value;
			var searchString = attributeMap.screenPopValue.value;
			var screenpopURL = baseURL + searchString;
			// window.open(screenpopURL);
			
			let newWindow = open(screenpopURL, 'Login into amazon connect', 'width=400,height=600');

			// FAClient.open();
			// var iframe = document.querySelectorAll('iframe[title="amazon_connect"]');
			// iframe.src = screenpopURL;
		});
	});

	// FAClient.on('open', () => {
	// 	if (!isAuthenticated) {
	// 		closeApplet();
	// 	}
	// });
};